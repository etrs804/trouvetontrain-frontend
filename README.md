# TrouveTonTrain
[![Build status](https://brassoud.visualstudio.com/trouvetontrain-frontend/_apis/build/status/trouvetontrain-frontend-CI)](https://brassoud.visualstudio.com/trouvetontrain-frontend/_build/latest?definitionId=4)


Ce dépôt contient l'ensemble du code pour la partie Frontend du projet TrouveTonTrain. Elle est disponible ici : https://trouvetontrain.azurewebsites.net/


## Dépendances

L'application a pour but de trouver un ensemble de trajets d'une gare à une autre pour une période donnée. Elle repose sur les différentes parties :
 * [Backend](https://gitlab.com/etrs804/trouvetontrain-django-backend) :
   * SOAP : Service SOAP utilisé pour calculer la distance d'une gare à l'autre
   * REST : Service utilisé pour calculer le coût estimé du trajet avec une devise donnée
 * API REST Navitia :
   * Trouver les gares
   * Trouver les trajets


## CI/CD

Chaque release sur la branche master est automatiquement déployée grâce aux pipelines Microsoft Azure.


## Build

Ce projet a été réalisé avec Angular 9. Cloner le dépôt puis exécuter `ng serve` pour déployer le serveur de développement en local. Puis rendez-vous sur `http://localhost:4200/` pour faire apparaître le site.


## Métadonnées

Xavier BRASSOUD (@XavierBrassoud) 2020 - Projet réalisé dans un cadre d'étude en Master 1 TRI à l'université Savoie Mont-Blanc - ETRS804 WebServices (David TELISSON)
