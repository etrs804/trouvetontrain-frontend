// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKeyNavitia: '91f4e074-1b00-4f1d-9ff7-ea55a145d591',
  backendUrl: 'http://localhost:8000',
  firebase: {
    apiKey: 'AIzaSyDoloaXSiusj5KLkT9vHYbdmqdxOYi9Nys',
    authDomain: 'trouvetontrain-272114.firebaseapp.com',
    databaseURL: 'https://trouvetontrain-272114.firebaseio.com',
    projectId: 'trouvetontrain-272114',
    storageBucket: 'trouvetontrain-272114.appspot.com',
    messagingSenderId: '371757617670',
    appId: "1:371757617670:web:25e1321d6bea9616882987",
    measurementId: "G-DBF59CHXZB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
