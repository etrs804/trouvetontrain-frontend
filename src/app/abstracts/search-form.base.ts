import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TrainStation } from '../models/train-station.model';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, switchMap, catchError } from 'rxjs/operators';
import { TrainStationFinderService } from '../services/train-station-finder.service';
import { IUserSearch, UserSearchesService } from '../services/user-searches.service';
import { AuthService } from '../services/auth.service';
import { NgbCalendar, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { IUser } from '../interfaces/IUser.interface';

/** Base class that handle search form */
export abstract class SearchFormBase {

  searchTrainForm: FormGroup;
  userSearchHistories: IUserSearch[];

  constructor(
    private trainStationFinder: TrainStationFinderService,
    private formBuilder: FormBuilder,
    public authService: AuthService,
    protected userSearchesService: UserSearchesService,
    protected ngbCalendar: NgbCalendar,
    protected dateAdapter: NgbDateAdapter<string>
  ) 
  { 
    this.dateAdapter = dateAdapter;

    this.searchTrainForm = this.formBuilder.group({
      from: ['', Validators.required],
      to: ['', Validators.required],
      date: ['', Validators.required],
      hour: ['', Validators.required],
      minute: ['', Validators.required],
    });

    // Si utilisateur connecté, on récupère son historique
    this.authService.user.subscribe(user => {
      if (user) {
        this.userSearchesService.searches.subscribe(searches => {
          this.userSearchHistories = searches;
        })
      }
    });
  }

  formatter = (city: TrainStation) => city ? city.name + (city.department ? ' (' + city.department + ')' : '') : null;

  search = (text$: Observable<string>) => text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    filter(term => term.length >= 3 && (!term.includes('(') && !term.includes(')'))),
    switchMap(async term => await this.trainStationFinder.trainStationLookup(term)),
    catchError(e => {
      throw new Error(e);
      // this.alerts.push({type: 'danger', message: 'Le service de recherche des gares est temporairement inaccessible.'});
    })
  )


  submitHistory(search: IUserSearch) {
    
    this.searchTrainForm.patchValue(
      {
        from: search.from,
        to: search.to,
        date: this.dateAdapter.toModel(this.ngbCalendar.getToday())!,
        hour: search.hour,
        minute: search.minute
      }
    );
  }

}