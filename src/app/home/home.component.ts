import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { TrainStationFinderService } from '../services/train-station-finder.service';
import { SearchFormBase } from '../abstracts/search-form.base';
import { AuthService } from '../services/auth.service';
import { UserSearchesService } from '../services/user-searches.service';
import { NgbDateAdapter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent extends SearchFormBase {

  constructor(
    private router: Router,
    formBuilder: FormBuilder,
    trainStationFinder: TrainStationFinderService,
    authService: AuthService,
    userSearchesService: UserSearchesService,
    ngbCalendar: NgbCalendar,
    dateAdapter: NgbDateAdapter<string>
  ) { 
    super(trainStationFinder, formBuilder, authService, userSearchesService, ngbCalendar, dateAdapter);
  }

  async onSubmit() {
    this.router.navigate(['results'], { state: { data: this.searchTrainForm.value } } );
  }

}
