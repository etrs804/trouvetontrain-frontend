/** Utilisateur authentifié */
export class IUser {
    uid: string;
    email: string;
    displayName: string;
  }