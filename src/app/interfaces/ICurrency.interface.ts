
export interface ICurrency {
    symbol: string;
    label: string;
}