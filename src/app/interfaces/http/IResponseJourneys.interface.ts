export interface IResponseJourneys {
    tickets: any[];
    links: Link[];
    journeys: Journey[];
    disruptions: any[];
    notes: any[];
    feed_publishers: FeedPublisher[];
    context: Context;
    exceptions: any[];
}

 interface Link {
    href: string;
    type: string;
    rel: string;
    templated: boolean;
}

 interface Distances {
    taxi: number;
    car: number;
    walking: number;
    bike: number;
    ridesharing: number;
}

 interface Link2 {
    href: string;
    type: string;
    rel: string;
    templated: boolean;
}

 interface Durations {
    taxi: number;
    walking: number;
    car: number;
    ridesharing: number;
    bike: number;
    total: number;
}

 interface ActivePeriod {
    begin: string;
    end: string;
}

 interface WeekPattern {
    monday: boolean;
    tuesday: boolean;
    friday: boolean;
    wednesday: boolean;
    thursday: boolean;
    sunday: boolean;
    saturday: boolean;
}

 interface Calendar {
    active_periods: ActivePeriod[];
    week_pattern: WeekPattern;
}

 interface Total {
    value: string;
}

 interface Fare {
    found: boolean;
    total: Total;
    links: any[];
}

 interface Co2Emission {
    value: number;
    unit: string;
}

 interface Coord {
    lat: string;
    lon: string;
}

 interface Coord2 {
    lat: string;
    lon: string;
}

 interface AdministrativeRegion {
    insee: string;
    name: string;
    level: number;
    coord: Coord2;
    label: string;
    id: string;
    zip_code: string;
}

 interface StopArea {
    name: string;
    links: any[];
    coord: Coord;
    label: string;
    administrative_regions: AdministrativeRegion[];
    timezone: string;
    id: string;
}

 interface Coord3 {
    lat: string;
    lon: string;
}

 interface Coord4 {
    lat: string;
    lon: string;
}

 interface AdministrativeRegion2 {
    insee: string;
    name: string;
    level: number;
    coord: Coord4;
    label: string;
    id: string;
    zip_code: string;
}

 interface FareZone {
    name: string;
}

 interface Coord5 {
    lat: string;
    lon: string;
}

 interface StopArea2 {
    name: string;
    links: any[];
    coord: Coord5;
    label: string;
    timezone: string;
    id: string;
}

 interface StopPoint {
    name: string;
    links: any[];
    coord: Coord3;
    label: string;
    equipments: any[];
    administrative_regions: AdministrativeRegion2[];
    fare_zone: FareZone;
    id: string;
    stop_area: StopArea2;
}

 interface From {
    embedded_type: string;
    stop_area: StopArea;
    quality: number;
    name: string;
    id: string;
    stop_point: StopPoint;
}

 interface Link3 {
    type: string;
    id: string;
}

 interface Co2Emission2 {
    value: number;
    unit: string;
}

 interface Coord6 {
    lat: string;
    lon: string;
}

 interface Coord7 {
    lat: string;
    lon: string;
}

 interface AdministrativeRegion3 {
    insee: string;
    name: string;
    level: number;
    coord: Coord7;
    label: string;
    id: string;
    zip_code: string;
}

 interface FareZone2 {
    name: string;
}

 interface Coord8 {
    lat: string;
    lon: string;
}

 interface StopArea3 {
    name: string;
    links: any[];
    coord: Coord8;
    label: string;
    timezone: string;
    id: string;
}

 interface StopPoint2 {
    name: string;
    links: any[];
    coord: Coord6;
    label: string;
    equipments: any[];
    administrative_regions: AdministrativeRegion3[];
    fare_zone: FareZone2;
    id: string;
    stop_area: StopArea3;
}

 interface Coord9 {
    lat: string;
    lon: string;
}

 interface Coord10 {
    lat: string;
    lon: string;
}

 interface AdministrativeRegion4 {
    insee: string;
    name: string;
    level: number;
    coord: Coord10;
    label: string;
    id: string;
    zip_code: string;
}

 interface StopArea4 {
    name: string;
    links: any[];
    coord: Coord9;
    label: string;
    administrative_regions: AdministrativeRegion4[];
    timezone: string;
    id: string;
}

 interface To {
    embedded_type: string;
    stop_point: StopPoint2;
    quality: number;
    name: string;
    id: string;
    stop_area: StopArea4;
}

 interface DisplayInformations {
    direction: string;
    code: string;
    network: string;
    links: any[];
    color: string;
    name: string;
    physical_mode: string;
    headsign: string;
    label: string;
    equipments: any[];
    text_color: string;
    commercial_mode: string;
    description: string;
}

 interface Property {
    length: number;
}

 interface Geojson {
    type: string;
    properties: Property[];
    coordinates: number[][];
}

 interface Coord11 {
    lat: string;
    lon: string;
}

 interface FareZone3 {
    name: string;
}

 interface StopPoint3 {
    name: string;
    links: any[];
    coord: Coord11;
    label: string;
    equipments: any[];
    fare_zone: FareZone3;
    id: string;
}

 interface StopDateTime {
    stop_point: StopPoint3;
    links: any[];
    arrival_date_time: string;
    additional_informations: any[];
    departure_date_time: string;
    base_arrival_date_time: string;
    base_departure_date_time: string;
}

 interface Section {
    from: From;
    links: Link3[];
    arrival_date_time: string;
    co2_emission: Co2Emission2;
    to: To;
    departure_date_time: string;
    duration: number;
    type: string;
    id: string;
    mode: string;
    additional_informations: string[];
    display_informations: DisplayInformations;
    base_arrival_date_time: string;
    base_departure_date_time: string;
    geojson: Geojson;
    data_freshness: string;
    stop_date_times: StopDateTime[];
}

 interface Journey {
    status: string;
    distances: Distances;
    links: Link2[];
    tags: string[];
    nb_transfers: number;
    durations: Durations;
    arrival_date_time: string;
    calendars: Calendar[];
    departure_date_time: string;
    requested_date_time: string;
    fare: Fare;
    co2_emission: Co2Emission;
    type: string;
    duration: number;
    sections: Section[];
}

 interface FeedPublisher {
    url: string;
    id: string;
    license: string;
    name: string;
}

 interface Co2Emission3 {
    value: number;
    unit: string;
}

 interface CarDirectPath {
    co2_emission: Co2Emission3;
}

 interface Context {
    timezone: string;
    current_datetime: string;
    car_direct_path: CarDirectPath;
}

export interface IResponseJourneys {
    tickets: any[];
    links: Link[];
    journeys: Journey[];
    disruptions: any[];
    notes: any[];
    feed_publishers: FeedPublisher[];
    context: Context;
    exceptions: any[];
}



