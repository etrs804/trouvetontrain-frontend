/** Réponse de l'API Here sur endpoint Autocomplete */
export interface IResponseHereAutocomplete {
    suggestions: Suggestion[];
}

interface Suggestion {
    label: string;
    language: string;
    countryCode: string;
    locationId: string;
    address: Address;
    matchLevel: string;     
}

interface Address {
    country: string;
    state: string;
    county: string;
    city: string;
    district: string;
    street: string;
    houseNumber: string;
    postalCode: string;
}