/** Réponse de l'API Here sur endpoint Geolocalisation */
export interface IResponseHereGeolocation {
    Response: Response;
}

interface Response {
    View: View[];
}

interface View {
    Result: Result[];
    ViewId: number;
}

interface Result {
    MatchLevel: string;
    MatchType: string;
    Location: Location;
}

interface Location {
    LocationId: string;
    LocationType: string;
    DisplayPosition: Position;
    MapView: MapView;
    Address: Address;
    NavigationPosition: Position[];   
}

interface Position {
    Latitude: number;
    Longitude: number;
}

interface MapView {
    TopLeft: Position;
    BottomRight: Position;   
}

interface Address {
    Country: string;
    State: string;
    County: string;
    City: string;
    District: string;
    Street: string;
    HouseNumber: string;
    PostalCode: string;
}