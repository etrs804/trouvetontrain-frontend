
 export interface IResponseTrainStation {
    nhits: number;
    parameters: Parameters;
    records: Record[];
}

 interface Parameters {
    dataset: string;
    timezone: string;
    q: string;
    rows: number;
    format: string;
    lang: string;
}

 interface Record {
    datasetid: string;
    recordid: string;
    fields: Fields;
    geometry: Geometry;
    record_timestamp: Date;
}

 interface Fields {
    gare_ut_libelle: string;
    gare_alias_libelle_noncontraint: string;
    pltf_alias_libelle_noncontraint: string;
    pltf_commune_code: string;
    pltf_longitude_entreeprincipale_wgs84: string;
    pltf_code: string;
    pltf_departement_numero: string;
    gare_etrangere_on: string;
    pltf_alias_libelle_fronton: string;
    pltf_departement_libellemin: string;
    wgs_84: number[];
    pltf_niveauservice_libelle: number;
    pltf_rg_libelle: string;
    pltf_adresse_cp: string;
    pltf_uic_code: number;
    gare_drg_on: string;
    pltf_segmentdrg_libelle: string;
    gare_agencegc_libelle: string;
    gare_regionsncf_libelle: string;
    pltf_latitude_entreeprincipale_wgs84: string;
    gare_ug_libelle: string;
    pltf_commune_libellemin: string;
    pltf_tvs_listecodes: string;
    gare_nbpltf: number;
}

 interface Geometry {
    type: string;
    coordinates: number[];
}