/** Représente un trajet */
export class Journey {
    
    leave: Date;
    arrival: Date;
    from: string; 
    to: string;

    sections: Section[];
    transfers: number;
    
};

export class Section {
    
    /** type de transport */
    transportType: string;
    direction: string;
    
    /** Voie / N° de ligne */
    code: string;

    leave: Date;
    arrival: Date;
    from: string; 
    to: string;
}