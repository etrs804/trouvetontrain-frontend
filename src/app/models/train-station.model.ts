/** Représente une gare avec son  nom et ses coordonnées géographique*/
export class TrainStation {
    name: string;
    department: number;
    latitude: number; 
    longitude: number;
    
    /** Token nécessaire pour connaître les coordonnées de la ville */
    locationId: number;
};