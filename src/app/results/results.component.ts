import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { TrainStationFinderService } from '../services/train-station-finder.service';
import { TrainStation } from '../models/train-station.model';
import { NgbTypeaheadSelectItemEvent, NgbDatepickerI18n, NgbDateAdapter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { DistanceCalculationService } from '../services/distance-calculation.service';
import { CurrencyConverterService } from '../services/currency-converter.service';
import { ICurrency } from '../interfaces/ICurrency.interface';
import { NavitiaService } from '../services/navitia.service';
import { Journey } from '../models/journey.model';
import { SearchFormBase } from '../abstracts/search-form.base';
import { UserSearchesService, IUserSearch } from '../services/user-searches.service';
import { AuthService } from '../services/auth.service';
import { strict } from 'assert';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./results.component.css']
})
export class ResultsComponent extends SearchFormBase implements OnInit {

  // Models
  from: TrainStation;
  to: TrainStation;
  distance: number;
  price: number;

  searching: boolean = false;

  currencies: ICurrency[] = [
    { symbol: '€', label: 'EUR' },
    { symbol: '$', label: 'USD' },
    { symbol: '£', label: 'GBP' },
    { symbol: '¥', label: 'JPY' },
    { symbol: '₣', label: 'CHF' },
    { symbol: 'zł', label: 'PLN' }
  ];
  currencySelected: ICurrency;
  journeys: Journey[];

  // Gestion des erreurs
  alerts: Alert[] = [];
  errors = false;
  invalidFields: { [key: string]: boolean }[] = [];

  alertPromotionModulo = 8;

  constructor(
    private distanceService: DistanceCalculationService,
    private currencyConverter: CurrencyConverterService,
    private navitiaService: NavitiaService,
    trainStationFinder: TrainStationFinderService,
    formBuilder: FormBuilder,
    authService: AuthService,
    userSearchesService: UserSearchesService,
    ngbCalendar: NgbCalendar,
    dateAdapter: NgbDateAdapter<string>
  ) { 
    super(trainStationFinder, formBuilder, authService, userSearchesService, ngbCalendar, dateAdapter);
  }

  async ngOnInit(): Promise<any> {

    // Défini la devise courante en euros
    this.currencySelected = this.currencies.find(c => c.label == 'EUR');

    // Récupère les données passé par le formulaire de la Home Page
    if (history.state.data) {
      this.searchTrainForm.patchValue(
        {
          from: history.state.data.from,
          to: history.state.data.to,
          date: history.state.data.date,
          hour: history.state.data.hour,
          minute: history.state.data.minute
        }
      );

      await this.onSubmit();
    }
  }

  async getCityFromPosition(e: NgbTypeaheadSelectItemEvent) {
    //this.searchTrainForm.patchValue({from: await this.trainStationFinder.getCityPosition(e.item)});
  }

  async getCityToPosition(e: NgbTypeaheadSelectItemEvent) {
    //this.searchTrainForm.patchValue({to: await this.trainStationFinder.getCityPosition(e.item)});
  }

  async changeCurrency(currency: ICurrency) {
    this.price = await this.currencyConverter.getPrice(currency.label, this.distance);
    this.currencySelected = currency;
  }

  async onSubmit() {

    // Si une valeur n'est pas renseignée.
    this.alerts = [];
    this.invalidFields = [];
    this.errors = false;

    if (this.searchTrainForm.invalid) {

      // Mise en forme CSS des champs invalide
      for (const name in this.searchTrainForm.controls) {
        if (this.searchTrainForm.controls[name].invalid) {
          this.invalidFields[name] = true;
        }
      }

      this.alerts.push({ type: 'danger', message: 'Certains champs ne sont pas valides.' });
      this.errors = true;

      return;
    }

    this.searching = true;

    // Process checkout data here
    this.from = this.searchTrainForm.value.from;
    this.to = this.searchTrainForm.value.to;


    this.distanceService.getDistance(this.from, this.to)
      .then(async distance => {
        this.distance = distance;

        try {
          this.price = await this.currencyConverter.getPrice(this.currencySelected.label, this.distance);
        } catch {
          this.alerts.push({ type: 'warning', message: 'Le service de calcul de prix est momentanément indisponible.' });
        }

      })
      .catch(() => {
        this.alerts.push({ type: 'warning', message: 'Le service de calcul de distances est momentanément indisponible.' });
      });


    if (typeof(this.searchTrainForm.value.date) === 'string') {
      this.searchTrainForm.value.date = this.dateAdapter.fromModel(this.searchTrainForm.value.date);
    }
    const leave = new Date(
      this.searchTrainForm.value.date.year,
      this.searchTrainForm.value.date.month - 1,
      this.searchTrainForm.value.date.day,
      this.searchTrainForm.value.hour,
      this.searchTrainForm.value.minute
    );

    await this.navitiaService.getJourneys(this.from, this.to, leave)
      .then(journeys => {
        this.journeys = journeys;

        // Save the journey on user history
        if (this.userSearchHistories) {
          
          try {

            const search : IUserSearch = {
              id : null,
              /* Object.assign : Fix FirebaseError: Function DocumentReference.set() called with invalid data. Unsupported field value: a custom object (found in field from)
                https://stackoverflow.com/a/48158848/9414882
              */
              from: Object.assign({}, this.searchTrainForm.value.from),
              to: Object.assign({}, this.searchTrainForm.value.to),
              hour: this.searchTrainForm.value.hour,
              minute: this.searchTrainForm.value.minute,
              updateAt: null
            }

            // Si moins de 5 recherche effectuées
            if (this.userSearchHistories.length < 5) {
              // On l'ajoute en DB
              this.userSearchesService.addSearch(search);
            } else {
              // Sinon, remplace la recherche la plus ancienne par la nouvelle (limite à 5 recherche par utilisateur)
              search.id = this.userSearchHistories[this.userSearchHistories.length - 1].id;
              this.userSearchesService.updateSearch(search);
            }

            
          } catch (e) {
            this.alerts.push({ type: 'warning', message: 'Le service de sauvegarde des recherches est momentanément indisponible.' });
            console.error(e);
          }
          
        } else {
          if (this.alertPromotionModulo % 10 === 0) {
            this.alerts.push({ type: 'secondary', message: 'Vous appréciez l\'application ? Connectez-vous et retrouvez toutes vos recherches où que vous soyez !' });
          }
          this.alertPromotionModulo = (this.alertPromotionModulo + 1) % 10
        }
      })
      .catch((e) => {
        this.alerts.push({ type: 'warning', message: 'Le service de recherche de voyages est momentanément indisponible.' });
        console.error(e);
      })
    ;

    this.searching = false;
  }

  close(alert: Alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }
}

// Gestion des erreurs
interface Alert {
  type: string;
  message: string;
}
