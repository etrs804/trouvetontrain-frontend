import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TrainStation } from '../models/train-station.model';
import { environment } from 'src/environments/environment';
import { IResponseJourneys } from '../interfaces/http/IResponseJourneys.interface';
import { map } from 'rxjs/operators';
import { Journey, Section } from '../models/journey.model';

const httpOptions = {
  headers: new HttpHeaders({
  })
};

@Injectable({
  providedIn: 'root'
})
export class NavitiaService {

  constructor(private http: HttpClient) { }

  async getJourneys(from: TrainStation, to: TrainStation, leave: Date): Promise<Journey[]> {

    //let endpoint = `https://api.navitia.io/v1/journeys?key=${environment.apiKeyNavitia}&from=stop_area%3ASIN%3ASA%3AOCE${from.locationId}&to=stop_area%3ASIN%3ASA%3AOCE${to.locationId}&datetime=${leave.toISOString()}&datetime_represents=departure&count=5`;
    let endpoint = `https://api.navitia.io/v1/journeys?key=${environment.apiKeyNavitia}&from=${from.longitude};${from.latitude}&to=${to.longitude};${to.latitude}&datetime=${leave.toISOString()}&datetime_represents=departure&count=5`;
    var res: Journey[] = []
    try {
      res = await this.http.get(endpoint, httpOptions).pipe(
        map(this.extractJourneys)).toPromise();
    } catch {}

    return res;
  }

  // Parse le résultat de l'API dans le format voulu
  private extractJourneys(res: IResponseJourneys): Journey[] {
    const result: Journey[] = [];
    console.log(res);

    const parseDate = function (navitiaDate: string): string {
      const temp =  navitiaDate.slice(0,4) + '-' + navitiaDate.slice(4,6) + '-' + navitiaDate.slice(6,8) 
        + 'T' 
        + navitiaDate.slice(9,11) + ':' + navitiaDate.slice(11,13) + ':' + navitiaDate.slice(13,15)
        + 'Z';

      return temp;
    }

    for (let journey of res.journeys) {
      var j = new Journey();

      // Peuple les infos de départ
      j.leave = new Date(parseDate(journey.departure_date_time));
      j.from = journey.sections[1].from.name;

      // Peuple les infos d'arriver
      j.arrival = new Date(parseDate(journey.arrival_date_time));
      j.to = journey.sections[journey.sections.length - 2].to.name;
      
      j.transfers = journey.nb_transfers;

      // Peuple les différents changements
      const sections: Section[] = [];
      for (let section of journey.sections) {
        if (section.type === 'public_transport') {
            var s = new Section();
            s.leave = new Date(parseDate(section.departure_date_time));
            s.from = section.from.name;
            s.arrival = new Date(parseDate(section.arrival_date_time));
            s.to = section.to.name;

            s.transportType = section.display_informations.commercial_mode;
            s.direction = section.display_informations.direction;
            s.code = section.display_informations.code;

            sections.push(s);
        }
        
      }

      j.sections = sections;

      result.push(j)
    }

    return result;
  }

}
