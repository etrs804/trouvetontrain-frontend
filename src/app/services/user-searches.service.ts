import { Injectable } from '@angular/core';
import { TrainStation } from '../models/train-station.model';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { firestore } from 'firebase'


/** On ne stock pas la date car la recherche peut se faire sur n'importe quelle date */
export interface IUserSearch {
  id: string;
  from: TrainStation;
  to: TrainStation;
  hour: number;
  minute: number;
  updateAt: firestore.FieldValue
}

@Injectable({
  providedIn: 'root'
})
export class UserSearchesService {

  // Liste des recherches
  searches: Observable<IUserSearch[]>;
  searchesCollection: AngularFirestoreCollection<IUserSearch>;

  private searchResult: IUserSearch[];

  userId: string;

  constructor(
    private store: AngularFirestore,
    private authService: AuthService
  ) 
  {
    // Détection de l'utilisateur authentifié
    this.authService.user.subscribe(user => {
      if (user) {
        this.emitSearches(user.uid);
      }
    });
  }

  emitSearches(uid: string): Observable<IUserSearch[]> {
    this.userId = uid;

    this.searchesCollection = this.store.collection(`searches/${this.userId}/history`, ref => ref.orderBy('updateAt', 'desc'));
    this.searches = this.searchesCollection.snapshotChanges().pipe(
      map(changes => {

         this.searchResult = changes.map(action => {
            const data = action.payload.doc.data() as IUserSearch;
            data.id = action.payload.doc.id;
            return Object.assign({}, data);
          })

          return this.searchResult;
      }
    ));

    return this.searches;
  }
  getSearches(): Observable<IUserSearch[]> {
    if (!this.userId) {
      return;
    }

    return this.searches;
  }

  addSearch(search: IUserSearch) {
    if (!this.userId) {
      return;
    }

    const similarSearch = this.findSimilarSearch(search);

    if (similarSearch) {
      // Update the updateAt field
      similarSearch.updateAt = firestore.FieldValue.serverTimestamp();
      const doc = this.store.doc(`searches/${this.userId}/history/${search.id}`);
      doc.update(Object.assign({}, search));
    } else {
      // Create history
      search.updateAt = firestore.FieldValue.serverTimestamp();
      this.searchesCollection.add(Object.assign({}, search));
    }
    
  }

  removeSearch(search: IUserSearch) {
    if (!this.userId) {
      return;
    }

    const doc = this.store.doc(`searches/${this.userId}/history/${search.id}`);
    doc.delete();
  }

  updateSearch(search: IUserSearch) {
    if (!this.userId) {
      return;
    }

    const similarSearch = this.findSimilarSearch(search);

    if (similarSearch) {
      // Update the updateAt field
      search = similarSearch;
    }

    search.updateAt = firestore.FieldValue.serverTimestamp();

    const doc = this.store.doc(`searches/${this.userId}/history/${search.id}`);
    doc.update(Object.assign({}, search));
  }

  private findSimilarSearch(search: IUserSearch): IUserSearch {
    return this.searchResult.find(s => 
          JSON.stringify(s.to) === JSON.stringify(search.to)
      &&  JSON.stringify(s.from) === JSON.stringify(search.from)
      &&  JSON.stringify(s.hour) === JSON.stringify(search.hour)
      &&  JSON.stringify(s.minute) === JSON.stringify(search.minute)
    );
  }
}
