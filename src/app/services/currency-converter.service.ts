import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CurrencyConverterService {

  constructor(private http: HttpClient) { }

  
  async getPrice(currency: string, km: number): Promise<number> {

    let endpoint = `${environment.backendUrl}/currency/`;

    const body = {
      currency: currency,
      km: km
    };

    const res = await this.http.post(endpoint, JSON.stringify(body)).toPromise();

    return parseFloat(res['result']);
  }
}
