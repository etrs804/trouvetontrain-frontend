import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TrainStation } from '../models/train-station.model';
import { IResponseTrainStation } from '../interfaces/http/IResponseTrainStation';

const httpOptions = {
  headers: new HttpHeaders({
  })
};

@Injectable({
  providedIn: 'root'
})

/** 
 * Classe de service utilisé pour trouver la gare sur saisie utilisateur
 * Consomme l'API ressources de la SNCF
 */
export class TrainStationFinderService {

  constructor(private http: HttpClient) { }
  
  /** ETAPE 1 : Interroge l'API SNCF afin d'obtenir les 5 premieres gares correspondantes à la saisie
   *  URL obtenu depuis la console SNCF : https://ressources.data.sncf.com/api/v1/console/records/1.0/search//?dataset=referentiel-gares-voyageurs&lang=fr&rows=5
   */
  async trainStationLookup(input: string): Promise<TrainStation[]> {

    let endpoint = `https://ressources.data.sncf.com/api/records/1.0/search//?dataset=referentiel-gares-voyageurs&q=${input}&lang=fr&rows=5`;

    return await this.http.get(endpoint, httpOptions).pipe(
      map(this.extractTrainStationLookup)).toPromise();
  }

  // Parse le résultat de l'API dans le format voulu
  private extractTrainStationLookup(res: IResponseTrainStation): TrainStation[] {
    const result: TrainStation[] = [];

    for (let record of res.records) {
      var trainStation = new TrainStation()
      trainStation.name = record.fields.gare_alias_libelle_noncontraint;
      trainStation.locationId = record.fields.pltf_uic_code;
      trainStation.department = parseInt(record.fields.pltf_departement_numero);

      trainStation.longitude = -1;
      trainStation.latitude = -1;
      if (record.geometry) {
        trainStation.longitude = record.geometry.coordinates[0];
        trainStation.latitude = record.geometry.coordinates[1];
      }
      
      result.push(trainStation)
    }

    return result;
  }
}
