import { Injectable } from '@angular/core';
import { NgxSoapService, Client, ISoapMethodResponse } from 'ngx-soap';
import { TrainStation } from '../models/train-station.model';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DistanceCalculationService {

  client: Client;

  constructor(private soap: NgxSoapService) {

    // Récupération de la configuration SOAP au près du serveur
    this.soap.createClient(`${environment.backendUrl}/calcul_distance/?wsdl`)
      .then(client => this.client = client)
      .catch(e => {
        console.log(e);
      });
  }

  async getDistance(from: TrainStation, to: TrainStation): Promise<number> {
    const body = {
      from_longitude: from.longitude,
      from_latitude: from.latitude,
      to_longitude: to.longitude,
      to_latitude: to.latitude
    };

    // Si le client n'a pas été recréé
    if (!this.client) {
      await this.soap.createClient(`${environment.backendUrl}/calcul_distance/?wsdl`).then(client => this.client = client);
    }

    // Appel de la fonction distante : calcul_distance
    const res = await this.client.call('calcul_distance', body).toPromise<ISoapMethodResponse>()
    return parseFloat(res.result.calcul_distanceResult);
  }
}
