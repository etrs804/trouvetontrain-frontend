import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IUser } from '../interfaces/IUser.interface';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { switchMap } from 'rxjs/operators';
import { auth, User } from 'firebase';
import { AngularFirestoreDocument } from '@angular/fire/firestore/public_api';

/** Service d'authentification FireBase (tuto vidéo : https://youtu.be/qP5zw7fjQgo) */
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<IUser>;
  
  constructor(
    private auth: AngularFireAuth,
    private store: AngularFirestore
  ) { 
      // Détection de l'utilisateur authentifié
      this.user = this.auth.authState.pipe(
        switchMap (user => {
          if (user) {
            return this.store.doc<IUser>(`users/${user.uid}`).valueChanges();
          } else {
            return of(null);
          }
        })
      );
  }

  async googleSignIn() {
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.auth.signInWithPopup(provider);
    return this.updateUserData(credential.user);
  }

  async signOut() {
    await this.auth.signOut();
  }

  updateUserData({ uid, email, displayName}: User) {
    const userRef: AngularFirestoreDocument<IUser> = this.store.doc(`users/${uid}`);

    const data = { uid, email, displayName};

    return userRef.set(data, {merge: true});
  }
}
